package com.example.strategy;

import com.example.strategy.stylewalki.WalkaKamieniem;
import com.example.strategy.stylewalki.WalkaMieczem;
import com.example.strategy.stylewalki.WalkaOwcą;
import com.example.strategy.stylewalki.WalkaŁukiem;

public class Main {
    public static void main(String[] args) {
        Bohater bohater = new Bohater("Zdzisław");
        bohater.setBroń(new WalkaMieczem());
        bohater.walcz();

        bohater.setBroń(new WalkaŁukiem());
        bohater.walcz();
        bohater.walcz();
        bohater.walcz();
        bohater.walcz();

        bohater.setBroń(new WalkaOwcą());
        bohater.walcz();

        bohater.setBroń(new WalkaKamieniem());
        bohater.walcz();


    }
}
