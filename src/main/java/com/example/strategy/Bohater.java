package com.example.strategy;

public class Bohater {
    private String imie;

    // wartość domyślna to null
    private StrategiaWalki broń;

    public Bohater(String imie) {
        this.imie = imie;
    }

    public void setBroń(StrategiaWalki broń) {
        this.broń = broń;
    }

    public void walcz(){
        broń.walcz();
    }
}
