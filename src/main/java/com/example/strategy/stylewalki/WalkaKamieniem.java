package com.example.strategy.stylewalki;

import com.example.strategy.StrategiaWalki;

public class WalkaKamieniem implements StrategiaWalki {
    @Override
    public void walcz() {
        System.out.println("Pew Pew rzut kamieniem!");
    }
}
