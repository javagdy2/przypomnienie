package com.example.strategy.stylewalki;

import com.example.strategy.StrategiaWalki;

public class WalkaŁukiem implements StrategiaWalki {
    public void walcz() {
        System.out.println("Szczelam szczałami!");
    }
}
