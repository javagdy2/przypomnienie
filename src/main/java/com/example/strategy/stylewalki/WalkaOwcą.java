package com.example.strategy.stylewalki;

import com.example.strategy.StrategiaWalki;

public class WalkaOwcą implements StrategiaWalki {
    public void walcz() {
        System.out.println("Podkładam owcę z prochem!");
    }
}
