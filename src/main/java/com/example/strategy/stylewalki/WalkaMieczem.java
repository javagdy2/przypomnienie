package com.example.strategy.stylewalki;

import com.example.strategy.StrategiaWalki;

public class WalkaMieczem implements StrategiaWalki {
    public void walcz() {
        System.out.println("Walczę mieczem.");
    }
}
