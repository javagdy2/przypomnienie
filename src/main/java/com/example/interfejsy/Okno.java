package com.example.interfejsy;

public class Okno implements IOtwieralny, IZamykalny {
    public void otworz() {
        System.out.println("Otwieram");
    }

    public void zamknij() {
        System.out.println("Zamykam");
    }
}
