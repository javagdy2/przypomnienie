package com.example.lombok;

import lombok.*;

//@Setter
//@Getter
//@EqualsAndHashCode()
//@ToString
//@RequiredArgsConstructor

@Data // ^ zawiera wszystkie powyższe
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    private Long id;
    private String imie;
    private String nazwisko;
    private boolean pelnoletni;

    @ToString.Exclude
    private double wzrost;

    
}
