package com.example.polimorfism;

public class Ptak {
    public void spiewaj(){
        System.out.println("ćwir ćwir");
    }

    public void spiewaj(String jakaNuta){
        System.out.println("la la la: " + jakaNuta);
    }
}


// Set
// HashSet - brak gwarancji kolejności
// LinkedHashSet -- zachowana kolejność wstawiania elementów
// TreeSet -- uporządkowanie / sortowanie elementów

// Map
// HashMap - brak gwarancji kolejności
// LinkedHashMap -- zachowana kolejność wstawiania elementów (klucze)
// TreeMap -- uporządkowany / posortowany zbiór elementów (uporządkowanie kluczy)


// List
// ArrayList
// LinkedList --