package com.example.polimorfism;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Bocian bocian = new Bocian();
        bocian.spiewaj();

        // kle kle

        Ptak ptak = new Kukułka();
        ptak.spiewaj();

        // ku ku

        Ptak ptaszek = new Ptak();
        ptaszek.spiewaj();

        // ćwir ćwir

        List<Ptak> list = new ArrayList<Ptak>();
        list.add(bocian);
        list.add(ptak);
        list.add(ptaszek);

        for (Ptak ptak1 : list) {
            ptak1.spiewaj();
        }

        list.stream().filter(e -> {
            return true;
        });
    }
}
