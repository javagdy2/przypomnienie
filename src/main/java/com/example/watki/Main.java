package com.example.watki;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        // miejsce w pamięci
        // pamięć ram (stos)
        // do dużych zadań, które się nie restartują
        Thread watek = new Thread(() -> {
            try {
                System.out.println("Zaczynam prace");
                Thread.sleep(1000);
                System.out.println("Wyspalem sie");
                Thread.sleep(1000);
                System.out.println("Koncze prace");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        watek.start();

        // pula wątków [wzorzec projektowy]
        // do małych zadań - pula wątków wybiera zadania z kolejki
        ExecutorService service = Executors.newSingleThreadExecutor();
        service.submit(() -> {
            try {
                System.out.println("Zaczynam prace");
                Thread.sleep(1000);
                System.out.println("Wyspalem sie");
                Thread.sleep(1000);
                System.out.println("Koncze prace");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
