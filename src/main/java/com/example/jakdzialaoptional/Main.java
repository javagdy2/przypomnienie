package com.example.jakdzialaoptional;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Optional<Integer> wartoscWynikowa = KalkulatorWartosci.oblicz(1, 2, 3);

        // sprawdzam czy posiadamy wartość wewnątrz
        if (wartoscWynikowa.isPresent()) { // jeśli jest wartość, to mogę ją odpakować metodą "GET"
            int wartosc = wartoscWynikowa.get();
            int przemnozonaWartosc = wartosc * 10;
            System.out.println("Wynik: " + przemnozonaWartosc);
        }
    }
}
