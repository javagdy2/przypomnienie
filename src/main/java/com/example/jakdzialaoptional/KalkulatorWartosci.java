package com.example.jakdzialaoptional;

import java.util.Optional;

public class KalkulatorWartosci {

    // wynik jest opcjonalny (może go nie być)
    public static Optional<Integer> oblicz(int... liczby) {
        if (liczby.length < 10) {
            return Optional.empty(); // nie ma wyniku, zwróć pusty optional
        }
        int wynik = 0;
        for (int i = 0; i < liczby.length; i++) {
            if (i % 2 == 0) {
                wynik += liczby[i];
            } else {
                wynik -= liczby[i];
            }
        }
        // Stwórz Optional i wstaw tam wynik - zwróć ten optional
        return Optional.of(wynik);
    }
}
