package com.example.trywithresources;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        File f = new File("plik.txt");
//        try {
//            Scanner scanner = new Scanner(f);
//
//            /// .. czytanie...
//
//            scanner.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

        try (Scanner scanner = new Scanner(f)){
            /// .. czytanie...

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
