package com.example.equalshash;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
//        Set<String> obiektyString = new HashSet<>();
//
//        obiektyString.add("abecadlo");
//        obiektyString.add("abecadlO");

        Set<Para> obiektyPary = new HashSet<>();
        obiektyPary.add(new Para(1, 2));
        obiektyPary.add(new Para(2, 1));
        obiektyPary.add(new Para(1, 1));
        obiektyPary.add(new Para(2, 2));

        System.out.println("Pomiar 1 : " + obiektyPary.size()); // 4

        // domyślnie klasa porównuje referencje
        obiektyPary.add(new Para(1, 2)); // hash = 10 // 4
        System.out.println("Pomiar 2 : " + obiektyPary.size()); // 5

        obiektyPary.add(new Para(1, 2)); // hash = 10 // 4
        System.out.println("Pomiar 3 : " + obiektyPary.size()); // 6


        // -- ==
        // -- .equals

        String pawel = "pawel";
        String innyPawel = new String("pawel");

        System.out.println("P1: " + (pawel == innyPawel));
        System.out.println("P2: " + (pawel.equals(innyPawel)));
    }
}
