package com.example.porownanieszybkosci;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            integers.add(i);
        }

        long timeStart = System.currentTimeMillis();
//        List<Integer> filtered = new ArrayList<>();
//        for (int i = 0; i < integers.size(); i++) {
//            if (i % 2 == 0) {
//                filtered.add(i);
//            }
//        }
        List<Integer> filtered = integers.stream().filter(integer -> integer % 2 == 0).collect(Collectors.toList());

        long timeStop = System.currentTimeMillis();
        System.out.println("Czas wykonania: " + (timeStop - timeStart));
    }
}
